app.controller("myCtrl", function($scope) {
    $scope.firstName = "John";
    $scope.lastName = "Doe";
});
app.controller("formController", function($scope) {
    $scope.master = {firstName: "John", lastName: "Doe"};
    $scope.reset = function() {
        $scope.user = angular.copy($scope.master);
    };
    $scope.reset();
});
app.controller("validateCtrl", function($scope) {
    $scope.user = 'John Doe';
    $scope.email = 'john.doe@gmail.com';
});