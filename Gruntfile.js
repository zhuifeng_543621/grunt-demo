module.exports = function(grunt) {
	// load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
	require('load-grunt-tasks')(grunt);
	// 构建任务配置
	grunt.initConfig({
		//读取package.json的内容，形成个json数据
		pkg: grunt.file.readJSON('package.json'),
		//Grunt 任务配置
		htmlhint: {
			build: {
				options: {
					'tag-pair': true,
					'tagname-lowercase': true,
					'attr-lowercase': true,
					'attr-value-double-quotes': true,
					'spec-char-escape': true,
					'id-unique': true,
					'head-script-disabled': true,
				},
				src: ['src/**/*.html']
			}
		},
		sass: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src',
					src: ['sass/**/*.scss'],
					dest: 'src/css/',
					ext: '.css'
				}]
			}
		},
		coffee: {
			compile: {
				options: {
					//去掉匿名函数包裹
					bare: true,
					//当编译多个.coffee文件成单个.js时，先合并
					join: true,
					//编译.coffee文件时生成个.map文件，用于链接到coffee源码文件
					sourceMap: true
				},
				files: [{
					expand: true,
					cwd: 'src',
					src: ['coffee/**/*.coffee'],
					dest: 'src/js/',
					ext: '.js'
				}]
			}
		},
		// JS语法检查
		jshint: {
			options: {
				//大括号包裹
				curly: true,
				//对于简单类型，使用===和!==，而不是==和!=
				eqeqeq: true,
				//对于首字母大写的函数（声明的类），强制使用new
				newcap: true,
				//禁用arguments.caller和arguments.callee
				noarg: true,
				//对于属性使用aaa.bbb而不是aaa['bbb']
				sub: true,
				//查找所有未定义变量
				undef: true,
				//查找类似与if(a = 0)这样的代码
				boss: true,
				//指定运行环境为node.js
				node: true
			},
			//具体任务配置
			files: {
				src: ['src/js/*.js']
			}
		},
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'src/',
					src: ['images/**/*.{png,jpg,gif}'],
					dest: 'src/imagemin/'
				}]
			}
		},
		yuidoc: {
			//paths：js源码位置；
			//themedir：文档模板位置；
			//outdir：文档输出位置。
			//http://ju.outofmemory.cn/entry/62898
			compile: {
				name: '<%= pkg.name %>',
				description: '<%= pkg.description %>',
				version: '<%= pkg.version %>',
				url: '<%= pkg.homepage %>',
				options: {
					paths: 'path/to/source/code/',
					themedir: 'path/to/custom/theme/',
					outdir: 'where/to/save/docs/'
				}
			}
		},
		connect: {
			options: {
				port: 9000,
				hostname: '*', //默认就是这个值，可配置为本机某个 IP，localhost 或域名
				livereload: 35729 //声明给 watch 监听的端口
			},
			server: {
				options: {
					open: true, //自动打开网页 http://
					base: [
						'src' //主目录
					]
				}
			}
		},
		watch: {
			livereload: {
				options: {
					livereload: '<%=connect.options.livereload%>' //监听前面声明的端口  35729
				},
				files: [ //下面文件的改变就会实时刷新网页
					'src/**/*.html',
					'src/sass/**/*.scss',
					'src/coffee/**/*.coffee',
					'src/images/{,*/}*.{png,jpg}'
				]
			},
			scripts: {
				files: ['src/coffee/**/*.coffee'],
				tasks: ['newer:coffee', 'newer:jshint']
			},
			html: {
				files: ['src/**/*.html'],
				tasks: ['newer:htmlhint']
			},
			css: {
				files: ['src/sass/**/*.scss'],
				tasks: ['newer:sass']
			},
		}
	});
	//默认的Grunt任务
	grunt.registerTask('default', ['newer:htmlhint', 'newer:coffee', 'connect', 'watch']);
	grunt.registerTask('test', ['newer:sass', 'watch']);
};