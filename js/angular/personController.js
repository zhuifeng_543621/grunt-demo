var personController = function($scope) {
    $scope.person = {
        firstName: "John",
        lastName: "Doe",
        fullName: function() {
            var x;
            x = $scope.person;
            return x.firstName + " " + x.lastName;
        }
    };
    $scope.myVar=false;
    $scope.toggle=function(){
        $scope.myVar=!$scope.myVar;
    };
    $scope.names = [
        {name:'Jani',country:'Norway'},
        {name:'Hege',country:'Sweden'},
        {name:'Kai',country:'Denmark'}
    ];
    // $scope.fullName: function() {
    //     var x;
    //     x = $scope.person;
    //     return x.firstName + " " + x.lastName;
    // }

};
